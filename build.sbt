name := "scalacourse"

version := "0.1"

scalaVersion := "2.12.6"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http"   % "10.1.1",
  "com.typesafe.akka" %% "akka-stream" % "2.5.11",
  "com.h2database" % "h2" % "1.4.197",
  "com.typesafe.akka" %% "akka-slf4j" % "2.5.11",
  "com.typesafe.akka" %% "akka-actor" % "2.5.11",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.0",
  "io.spray" %% "spray-json" % "1.3.4",
  "org.tpolecat" %% "doobie-core" % "0.5.1",
  "org.scalikejdbc" %% "scalikejdbc" % "3.2.1",
  "org.scalikejdbc" %% "scalikejdbc-config" % "3.2.1",
  "org.scalikejdbc" %% "scalikejdbc-test" % "3.2.1" % "test",
  "com.typesafe.akka" %% "akka-http-testkit" % "10.1.1",
  "com.typesafe.akka" %% "akka-testkit" % "2.5.11" % "test",
  "org.scalatest" %% "scalatest" % "3.0.3" % "test",
  "com.github.nscala-time" %% "nscala-time" % "2.20.0"

)
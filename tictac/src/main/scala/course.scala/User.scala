package course.scala

import course.scala.models.UserReg
import scalikejdbc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

final case class User(id: Int, username: String, passwd: String, online : Int, wins : Int, losses : Int)

case class UserReg(username: String, password:String)
case class UserInfo(username: String, online :Boolean, wins: Int, losses: Int)


object User extends SQLSyntaxSupport[models.User] {

  override val tableName = "t_user"

  def apply(r: ResultName[models.User])(rs: WrappedResultSet) =
    new models.User(
      rs.int(r.id),
      rs.string(r.username),
      rs.string(r.passwd),
      rs.int(r.online),
      rs.int(r.wins),
      rs.int(r.losses)
    )

  private val t = syntax

  def create(user: models.User)(implicit session: DBSession = AutoSession): Future[Boolean] = {
    val sql = withSQL(insert.into(models.User).namedValues(
      column.id -> user.id,
      column.username -> user.username,
      column.passwd -> sha256Hash(user.passwd.toString),
      column.online -> user.online,
      column.wins -> user.wins,
      column.losses -> user.losses
    ))
    Future {
      sql.update().apply() == 1
    }
  }

  def register(userreg: UserReg)(implicit session: DBSession = AutoSession): Future[Boolean] = {
    val sql = withSQL(insert.into(models.User).namedValues(
     // column.id -> user.id,
      column.username -> userreg.username,
      column.passwd -> sha256Hash(userreg.password.toString),
      column.online -> 0,
      column.wins -> 0,
      column.losses -> 0
    ))
    Future {
      sql.update().apply() == 1
    }
  }

  def findById(username: String)(implicit s: DBSession = AutoSession): Future[Option[models.User]] = Future {
    val sql = withSQL(
      select.from[models.User](models as t).where.eq(t.username, username))
    sql.map(models.User(t.resultName)).headOption().apply()
  }

  private def sha256Hash(text: String) : String = String.format("%064x",
    new java.math.BigInteger(1, java.security.MessageDigest.getInstance("SHA-256").digest(text.getBytes("UTF-8"))))
}

object UserInfo extends SQLSyntaxSupport[models.UserInfo] {

  override val tableName = "t_user"

  def apply(r: ResultName[models.UserInfo])(rs: WrappedResultSet) =
    new models.UserInfo(
      rs.string(r.username),
      online = rs.boolean(r.online),
      rs.int(r.wins),
      rs.int(r.losses)
    )

  private val m = syntax

  def findById(username: String)(implicit s: DBSession = AutoSession): Future[Option[models.UserInfo]] = Future {
    val sql = withSQL(
      select.from[models.UserInfo](models.UserInfo as m).where.eq(m.username, username))
    sql.map(models.UserInfo(m.resultName)).headOption().apply
  }

}

package course.scala

import course.scala.models.{User, UserInfo, UserReg}
import scalikejdbc.DB

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

trait UserRepository {
  def createUser(user: User): Future[Unit]
  def registerUser(userreg: UserReg): Future[Unit]
  def getUser(username: String): Future[Option[User]]
  def getUserInfo(username: String) :Future[Option[UserInfo]]
}

object DBUserRepository extends UserRepository {
  TictacDb.init()
  Await.result(TictacDb.createTablesAndSomeData(), Duration.Inf)
  override def createUser(user: User): Future[Unit] = DB.futureLocalTx(implicit session => models.create(user).map(_ => ()))
  override def registerUser(userreg: UserReg): Future[Unit] = DB.futureLocalTx(implicit session => models.register(userreg).map(_ => ()))
  override def getUser(username: String): Future[Option[User]] = DB.futureLocalTx(implicit session => models.findById(username))
  override def getUserInfo(username: String): Future[Option[UserInfo]] = DB.futureLocalTx(implicit session => UserInfo.findById(username))
}

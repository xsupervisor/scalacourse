package course.scala

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directive0
import akka.http.scaladsl.server.Directives._
import course.scala.models.{UserInfo, UserReg}
import scalikejdbc.DB
import spray.json._

import scala.concurrent.ExecutionContext.Implicits.global

trait GeneralJsonSupport extends DefaultJsonProtocol with SprayJsonSupport {

  implicit val colorFormat = new JsonFormat[Color] {
    override def read(json: JsValue): Color = Color.fromString(json.convertTo[String])

    override def write(obj: Color): JsValue = JsString(obj.toString)
  }

  implicit val teapotFormat = jsonFormat6(Teapot.apply)
  implicit val userFormat = jsonFormat6(models.apply)
  implicit val userregFormat = jsonFormat2(UserReg.apply)
  implicit val userinfoFormat = jsonFormat4(UserInfo.apply)
}

trait WithAuth {
  def withAuth: Directive0 = optionalHeaderValueByName("auth").flatMap {
    case Some(k) if k == "123" => pass
    case _ => complete(StatusCodes.Unauthorized)
  }
}


trait Api extends GeneralJsonSupport with WithAuth {

  val teapotRepository: TeapotRepository
  val userRepository: UserRepository

  val route =
    pathPrefix("teapot") {
      path(LongNumber) { id =>
        withAuth {
          get {
            parameterMap { paramsMap =>
              onSuccess(teapotRepository.getTeapot(id)) {
                case Some(teapot) =>
                  complete(StatusCodes.ImATeapot -> JsObject(teapot.toJson.asJsObject.fields ++ Map("params" -> paramsMap.toJson)))
                case None =>
                  complete(StatusCodes.NotFound)
              }
            }
          }
        }
      } ~ post {
        entity(as[Teapot]) { teapot =>
          complete {
            teapotRepository.createTeapot(teapot).map(_ => StatusCodes.OK)
          }
        }
      }
    } ~
      pathPrefix("user") {
        path(Segment) { username =>
          withAuth {
            get {

              parameterMap { paramsMap =>
                onSuccess(userRepository.getUserInfo(username)) {
                  case Some(userInfo) =>
                    complete(StatusCodes.OK -> JsObject(userInfo.toJson.asJsObject.fields))
                  case None =>
                    complete(StatusCodes.NotFound)
                }
              }
            }
          }
        } ~ post {

            pathPrefix("login") {
            entity(as[UserReg]) { userReg =>
              complete("User Login")
            }
        } ~ pathPrefix("logout") {
              entity(as[UserReg]) { userReg =>
                complete("User Logout")
              }
            } ~ entity(as[UserReg]) { userReg =>
          complete {
            userRepository.registerUser(userReg).map(_ => StatusCodes.OK)
          }
        }

        }

      }


}

package course.scala

import course.scala.models.User
import scalikejdbc.{ConnectionPool, _}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object TictacDb {
  def init(): Unit = {
    Class.forName("org.h2.Driver")
    // ConnectionPool.singleton("jdbc:h2:mem:teapot;DB_CLOSE_DELAY=-1", "", "")
    // ConnectionPool.singleton("jdbc:h2:file:~/teapot", "sa", "")
    ConnectionPool.singleton("jdbc:h2:tcp://localhost/~/tictac", "sa", "")
  }

  def createTablesAndSomeData(): Future[Boolean] = {
    DB futureLocalTx { implicit session =>
      sql"""
      create table if not exists teapots (
        id serial not null primary key,
        volume int not null,
        max_temperature int not null,
        power float not null,
        color varchar (20) not null,
        backlight varchar (20) null)
      """.execute.apply()
      sql"""
      truncate table teapots
      """.execute.apply()

      Teapot.create(Teapot(1, 1, 100, 1.5, Color.Blue, Some(Color.Green)))

      sql"""
      drop table if exists t_user
      """.execute.apply()

      sql"""
      create table if not exists t_user (
        id serial not null primary key,
        username                  varchar(20) not null,
        passwd                    varchar(64),
        online                    integer,
        wins                      integer,
        losses                    integer)
      """.execute.apply()

      sql"""
      truncate table t_user
      """.execute.apply()

      models.create(User(1, "vasya", "test", 1, 3, 1))
      models.create(User(2, "nagibator", "test", 0, 2, 5))
      models.create(User(3, "otgibator", "test", 1, 4, 2))
      models.create(User(4, "kiska", "test", 0, 5, 7))
      models.create(User(5, "tetris", "test", 1, 7, 9))
      models.create(User(6, "tuborg", "test", 1, 9, 3))
      models.create(User(7, "doctor", "test", 0, 3, 1))
      models.create(User(8, "gamerx", "test", 0, 4, 6))

      sql"""
      drop table if exists t_session
      """.execute.apply()

      sql"""
      create table if not exists t_session(
        id serial not null primary key,
        uuid                      varchar(64) not null,
        user_id                   integer,
        is_active                 integer,
        created_at                timestamp,
        extended_at               timestamp)
      """.execute.apply()

      sql"""
      truncate table t_session
      """.execute.apply()

      sql"""
      drop table if exists t_game
      """.execute.apply()

      sql"""
      create table if not exists t_game(
        id serial not null primary key,
        sizex                     integer,
        sizey                     integer,
        crosses_length_to_win     integer,
        next_step                 integer,
        player_one                integer,
        player_two                integer,
        first_step                integer,
        won                       integer,
        is_finished               integer,
        step_counter              integer,
        created_at                timestamp,
        updated_at                timestamp,
        gamefield                 varchar(120))
      """.execute.apply()

      Future { sql"""
      truncate table t_game
      """.execute.apply() }
    }
  }
}

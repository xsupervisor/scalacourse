package course.scala

import course.scala.models.{Game, User, UserSession}
import scalikejdbc.{ConnectionPool, _}
import akka.http.javadsl.model.DateTime
import java.sql.Timestamp
import java.util.Calendar

import akka.http.scaladsl.model.headers.Date

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object TictacDb {
  def init(): Unit = {
    Class.forName("org.h2.Driver")
    ConnectionPool.singleton("jdbc:h2:mem:tictac;DB_CLOSE_DELAY=-1", "", "")
    // ConnectionPool.singleton("jdbc:h2:file:~/teapot", "sa", "")
    // ConnectionPool.singleton("jdbc:h2:tcp://localhost/~/tictac", "sa", "")
  }

  def createTicTacDB(): Future[Boolean] = {
    DB futureLocalTx { implicit session =>

      sql"""
      drop table if exists t_user
      """.execute.apply()

      sql"""
      create table if not exists t_user (
        id serial not null primary key,
        username                  varchar(20) not null,
        passwd                    varchar(64),
        online                    integer,
        wins                      integer,
        losses                    integer,
        created_at                timestamp)
      """.execute.apply()

      sql"""
      truncate table t_user
      """.execute.apply()

      User.create(User(1, "vasya", "test", 1, 3, 1))
      User.create(User(2, "nagibator", "test", 0, 2, 5))
      User.create(User(3, "otgibator", "test", 1, 4, 2))
      User.create(User(4, "kiska", "test", 0, 5, 7))
      User.create(User(5, "tetris", "test", 1, 7, 9))
      User.create(User(6, "tuborg", "test", 1, 9, 3))
      User.create(User(7, "doctor", "test", 0, 3, 1))
      User.create(User(8, "gamerx", "test", 0, 4, 6))

      sql"""
      drop table if exists t_session
      """.execute.apply()

      sql"""
      create table if not exists t_session(
        id serial not null primary key,
        uuid                      varchar(64) not null,
        user_id                   integer,
        is_active                 integer,
        created_at                timestamp,
        extended_at               timestamp)
      """.execute.apply()

      sql"""
      truncate table t_session
      """.execute.apply()

      sql"""
      drop table if exists t_game
      """.execute.apply()

      sql"""
      create table if not exists t_game(
        id serial not null primary key,
        sizex                     integer,
        sizey                     integer,
        crosses_length_to_win     integer,
        next_step                 integer,
        player_one                integer,
        player_two                integer,
        first_step                integer,
        won                       integer,
        is_finished               integer,
        step_counter              integer,
        created_at                timestamp,
        updated_at                timestamp,
        gamefield                 varchar(255))
      """.execute.apply()

      Future { sql"""
      truncate table t_game
      """.execute.apply() }
    }
  }
  def addTicTacDB(): Future[Boolean] = {
    DB futureLocalTx { implicit session =>

      UserSession.create(UserSession(1, "123456", 1, 1,  Timestamp.valueOf("2018-06-07 20:12:02"),
        Timestamp.valueOf("2018-06-07 21:48:15")))

      UserSession.create(UserSession(2, "34323432", 1, 1,  Timestamp.valueOf("2018-06-07 20:12:02"),
        new java.sql.Timestamp(Calendar.getInstance().getTime().getTime())))

      Game.create(Game(1,3,3,3,1,2,4,1,1,0,5, Seq(Seq(0,1,0), Seq(2,0,0), Seq(0,1,2)),Timestamp.valueOf("2018-06-07 20:12:02"),
        Timestamp.valueOf("2018-06-07 22:12:02")))

      Game.create(Game(2,2,4,2,1,3,1,1,1,0,5, Seq(Seq(0,0,0), Seq(0,0,0), Seq(0,0,0)),Timestamp.valueOf("2017-06-07 20:12:02"),
        Timestamp.valueOf("2017-06-07 22:12:02")))

    }
  }

}


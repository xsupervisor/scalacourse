package course.scala.models

import java.sql.Timestamp
import java.util.Calendar

import akka.http.javadsl.model.DateTime

import scala.util.Success
import java.util.UUID.randomUUID

import scalikejdbc._

import scala.util.{Failure, Success, Try}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}

final case class UserSession(id: Int, uuid: String, user_id: Int, is_active: Int, created_at : Timestamp, extended_at :Timestamp)

// case class UserReg(username: String, password:String)
// case class UserInfo(username: String, online :Boolean, wins: Int, losses: Int)


object UserSession extends SQLSyntaxSupport[UserSession] {

  override val tableName = "t_session"

  def apply(r: ResultName[UserSession])(rs: WrappedResultSet) =
    new UserSession(
      rs.int(r.id),
      rs.string(r.uuid),
      rs.int(r.user_id),
      rs.int(r.is_active),
      rs.timestamp(r.created_at),
      rs.timestamp(r.extended_at)
    )

  private val b = syntax
  private val w = User.w

  def create(userSession: UserSession)(implicit session: DBSession = AutoSession): Future[Boolean] = {
    val sql = withSQL(insert.into(UserSession).namedValues(
     // column.id -> userSession.id,
      column.uuid -> userSession.uuid,
      column.user_id -> userSession.user_id,
      column.is_active -> 1,
      column.created_at -> userSession.created_at,
      column.extended_at -> userSession.extended_at
    ))
    Future {
      sql.update().apply() == 1
    }
  }


  def findByUid(uid: String)(implicit s: DBSession = AutoSession): Future[Option[UserSession]] = Future {
    val sql = withSQL(
      select.from[UserSession](UserSession as b).where.eq(b.uuid, uid))
    sql.map(UserSession(b.resultName)).headOption().apply()
  }

  //select 1 from t_session WHERE  datediff('SECOND', extended_at, now())  <1000
  //select.from[UserSession](UserSession as t)

  def findSession(uid: String)(implicit s: DBSession = AutoSession): Future[Option[UserSession]] = Future {
    val sql = withSQL(
      select.from[UserSession](UserSession as b).innerJoin(User as w).on(w.column("id"), b.column("user_id"))
        .where.eq(b.column("uuid"), uid).and.eq(b.column("is_active"),1).and.append(sqls"datediff('SECOND', extended_at, now())  <10")
    )
    val res = sql.map(UserSession(b.resultName)).headOption().apply()
    if (res.isDefined) extSession(uid)
    res
  }

  def extSession(uid: String)(implicit s: DBSession = AutoSession): Future[Boolean] = Future {
    sql"update t_session set extended_at = CURRENT_TIMESTAMP()  where uuid = ${uid}".update.apply() == 1
  }

  def closeSession(uid: String)(implicit s: DBSession = AutoSession): Future[Boolean] = Future {
    sql"update t_session set is_active = 0 where uuid = ${uid}".update.apply() == 1
  }

  def checkSession(uid: Option[String])(implicit s: DBSession = AutoSession): Future[Option[UserSession]] =  {
     uid.map(findSession(_)).getOrElse(Future(None))
   }

/*
  def logIn(userreg: UserReg)(implicit session: DBSession = AutoSession): Future[String] =  {

    val IsUserSQL = withSQL(
      select.from[User](User as w).where.eq(w.column("username"), userreg.username)
    ).map(User(w.resultName)).headOption().apply()

    if (IsUserSQL.isDefined && (IsUserSQL.get.passwd.toString == User.sha256Hash(userreg.password))) {

      val newUid = randomUUID().toString

      UserSession.create(UserSession(1, newUid , IsUserSQL.get.id, 1,
        new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()),
        new java.sql.Timestamp(Calendar.getInstance().getTime().getTime())
      ))
      Future.successful(newUid)
    } else {
      Future.successful( "-1")
    }
  } */

  case class UserNotFoundException (mess: String ) extends Exception

  def logIn(userreg: UserReg)(implicit session: DBSession = AutoSession): Future[String] =
    Future {
      val IsUserSQL = withSQL(
        select.from[User](User as w).where.eq(w.column("username"), userreg.username)
      ).map(User(w.resultName)).headOption().apply()

      val newUid =
        IsUserSQL.map( user=> user.passwd.toString == User.sha256Hash(userreg.password))
          .map(_ =>randomUUID().toString).getOrElse(throw  UserNotFoundException("User not found"))

      val sessionTime = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime())

      UserSession(1, newUid , IsUserSQL.get.id, 1, sessionTime, sessionTime)

    }.flatMap{ uSession =>
      UserSession.create(uSession).map( f => if (f) uSession.uuid else "-1")
    }
}

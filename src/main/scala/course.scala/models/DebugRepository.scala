package course.scala.models

import course.scala.models
import scalikejdbc._
import course.scala.TictacDb

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object Debug extends SQLSyntaxSupport[Unit] {

  def clearDB(): Future[Boolean] = {
    DB futureLocalTx { implicit session =>
      sql"""
      TRUNCATE TABLE t_game
      """.execute.apply()

      sql"""
      ALTER TABLE t_game ALTER COLUMN id RESTART WITH 1
        """.execute.apply()

      sql"""
      TRUNCATE TABLE t_user
      """.execute.apply()

      sql"""
      ALTER TABLE t_user ALTER COLUMN id RESTART WITH 1
        """.execute.apply()

      sql"""
      TRUNCATE TABLE t_session
      """.execute.apply()

      sql"""
      ALTER TABLE t_session ALTER COLUMN id RESTART WITH 1
        """.execute.apply()

      Future.successful(true)
    }
  }

}
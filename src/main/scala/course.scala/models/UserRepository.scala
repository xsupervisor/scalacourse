package course.scala.models

import course.scala.models
import scalikejdbc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

final case class User(id: Int, username: String, passwd: String, online : Int, wins : Int, losses : Int)

case class UserReg(username: String, password:String)
case class UserInfo(username: String, online :Boolean, wins: Int, losses: Int)


object User extends SQLSyntaxSupport[User] {

  override val tableName = "t_user"

  def apply(r: ResultName[User])(rs: WrappedResultSet) =
    new User(
      rs.int(r.id),
      rs.string(r.username),
      rs.string(r.passwd),
      rs.int(r.online),
      rs.int(r.wins),
      rs.int(r.losses)
    )

  val w = syntax

  def create(user: User)(implicit session: DBSession = AutoSession): Future[Boolean] = {

    val sql = withSQL(insert.into(User).namedValues(
      column.id -> user.id,
      column.username -> user.username,
      column.passwd -> sha256Hash(user.passwd.toString),
      column.online -> user.online,
      column.wins -> user.wins,
      column.losses -> user.losses
    ))
    Future {
      sql.update().apply() == 1
    }
  }

  def register(userreg: UserReg)(implicit session: DBSession = AutoSession): Future[Int] = Future {

    val IsUserSQL = withSQL(
      select.from[User](User as w).where.eq(w.column("username"), userreg.username)
    ).map(User(w.resultName)).headOption().apply()

    if (IsUserSQL.isDefined) {
      -1
    } else {

      val sql = withSQL(insert.into(User).namedValues(
        // column.id -> user.id,
        column.username -> userreg.username,
        column.passwd -> sha256Hash(userreg.password.toString),
        column.online -> 0,
        column.wins -> 0,
        column.losses -> 0
      ))
      sql.update().apply()
      1
    }
  }

  def findById(username: String)(implicit s: DBSession = AutoSession): Future[Option[User]] = Future {
    val sql = withSQL(
      select.from[User](User as w).where.eq(w.username, username))
    sql.map(User(w.resultName)).headOption().apply()
  }

  def findByIdent(id: Int)(implicit s: DBSession = AutoSession): Future[Option[User]] = Future {
    val sql = withSQL(
      select.from[User](User as w).where.eq(w.id, id))
    sql.map(User(w.resultName)).headOption().apply
  }

  def sha256Hash(text: String) : String = String.format("%064x",
    new java.math.BigInteger(1, java.security.MessageDigest.getInstance("SHA-256").digest(text.getBytes("UTF-8"))))
}

object UserInfo extends SQLSyntaxSupport[UserInfo] {

  override val tableName = "t_user"

  def apply(r: ResultName[UserInfo])(rs: WrappedResultSet) =
    new UserInfo(
      rs.string(r.username),
      online = rs.boolean(r.online),
      rs.int(r.wins),
      rs.int(r.losses)
    )

  val m = syntax

  def findById(username: String)(implicit s: DBSession = AutoSession): Future[Option[UserInfo]] = Future {
    val sql = withSQL(
      select.from[UserInfo](UserInfo as m).where.eq(m.username, username))
    sql.map(UserInfo(m.resultName)).headOption().apply
  }

  def list(limit: Int, offset : Int)(implicit s: DBSession = AutoSession): Future[Option[List[UserInfo]]] = Future {
    val userList: List[UserInfo] = sql"""
        select u.username, u.online, u.wins, u.losses
        from t_user u
        limit ${limit} offset ${offset}
        """
      .stripMargin
      .map(rs =>
        new UserInfo(
          rs.string("username"),
          rs.boolean("online"),
          rs.int("wins"),
          rs.int("losses")
        )
      )
      .list
      .apply()

    Option(userList)

  }

}

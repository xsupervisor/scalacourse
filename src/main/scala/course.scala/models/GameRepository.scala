package course.scala.models

import java.sql.Timestamp
import java.util.Calendar

import scalikejdbc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future}

final case class Game(id: Int, sizex: Int, sizey: Int, crosses_length_to_win: Int,
                      next_step: Int, player_one: Int, player_two: Int, first_step: Int,
                      won: Int, is_finished: Int, step_counter: Int, gamefield: Seq[Seq[Int]],
                      created_at: Timestamp, updated_at: Timestamp)


case class GameInfo(id: Int, size: List[Int], finished: Boolean, won: String, players: Seq[String],next_step: String,
                    crosses_length_to_win: Int, steps: Int, field: Seq[Seq[Int]])

case class GameReg(opponent: String, size: Seq[Int], first_step_by : String, crosses_length_to_win : Int)

case class GameTurnRequest (step: Seq[Int])

case class InvalidPlayerException(message: String) extends Exception(message)
case class WrongTurnException(message: String) extends Exception(message)
case class CellNonEmptyException(message: String) extends Exception(message)
case class OpponentNotFoundException(message: String) extends Exception(message)
case class StepOutOfDeckException(message: String) extends Exception(message)


object Game extends SQLSyntaxSupport[Game] {

  import Helpers._

  override val tableName = "t_game"

  def apply(r: ResultName[Game])(rs: WrappedResultSet) =
    new Game(
      rs.int(r.id),
      rs.int(r.sizex),
      rs.int(r.sizey),
      rs.int(r.crosses_length_to_win),
      rs.int(r.next_step),
      rs.int(r.player_one),
      rs.int(r.player_two),
      rs.int(r.first_step),
      rs.int(r.won),
      rs.int(r.is_finished),
      rs.int(r.step_counter),
      rs.string(r.gamefield),
      rs.timestamp(r.created_at),
      rs.timestamp(r.updated_at)
    )

  private val t = syntax
  private val w = User.w
  private val b = UserSession.syntax


  def create(game: Game)(implicit session: DBSession = AutoSession): Future[Boolean] = {
    val sql = withSQL(insert.into(Game).namedValues(
      column.id -> game.id,
      column.sizex -> game.sizex,
      column.sizey -> game.sizey,
      column.crosses_length_to_win -> game.crosses_length_to_win,
      column.next_step -> game.next_step,
      column.player_one -> game.player_one,
      column.player_two -> game.player_two,
      column.first_step -> game.first_step,
      column.won -> game.won,
      column.is_finished -> game.is_finished,
      column.step_counter -> game.step_counter,
      column.gamefield -> game.gamefield,
      column.created_at -> game.created_at,
      column.updated_at -> game.updated_at
    ))
    Future {
      sql.update().apply() == 1
    }
  }


  def register(sess: Option[String], gamereg: GameReg)(implicit session: DBSession = AutoSession): Future[Int] = Future {

    val IsUserSQL = withSQL(
      select.from[User](User as w).where.eq(w.column("username"), gamereg.opponent)
    ).map(User(w.resultName)).headOption().apply()

    if (IsUserSQL.isEmpty) {
      throw OpponentNotFoundException("Opponent for this game is not found")
    } else {

      val fullSess = withSQL(
        select.from[UserSession](UserSession as b).where.eq(b.column("uuid"), sess)
      ).map(UserSession(b.resultName)).headOption().apply()

      if (fullSess.get.user_id == IsUserSQL.get.id) {
        throw OpponentNotFoundException("Opponent for this game is the same user")
      } else {

        val currUser = withSQL(
          select.from[User](User as w).where.eq(w.column("id"), fullSess.get.user_id)
        ).map(User(w.resultName)).headOption().apply()

        if (gamereg.first_step_by != currUser.get.username && gamereg.first_step_by != IsUserSQL.get.username) {
          throw OpponentNotFoundException("First step parameter is not related to any player")
        } else {

        val sessionTime = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime())

        def nextStep() : Int = { if (gamereg.first_step_by == currUser.get.username ) { currUser.get.id }
          else  IsUserSQL.get.id }

        //case class GameReg(opponent: String, size: Seq[Int], first_step_by : String, crosses_length_to_win : Int)

        val sql = withSQL(insert.into(Game).namedValues(
          // column.id -> user.id,
          column.sizex -> gamereg.size.head,
          column.sizey -> gamereg.size(1),
          column.crosses_length_to_win -> gamereg.crosses_length_to_win,
          column.next_step -> nextStep,
          column.player_one -> fullSess.get.user_id,
          column.player_two -> IsUserSQL.get.id,
          column.first_step -> nextStep,
          column.won -> null,
          column.is_finished -> false,
          column.step_counter -> 0,
          column.gamefield -> packFields(Seq.fill(gamereg.size(1), gamereg.size.head)(0)),
          column.created_at -> sessionTime,
          column.updated_at -> sessionTime

        ))
        sql.update().apply()
        1
      }
    }
      }
  }


  def findById(id: Int)(implicit s: DBSession = AutoSession): Future[Option[Game]] = Future {
    val sql = withSQL(
      select.from[Game](Game as t).where.eq(t.id, id))
    sql.map(Game(t.resultName)).headOption().apply()
  }

  case class GameInfo(id: Int, next_step: String, won: String, finished: Boolean,
                      players: Seq[String], steps: Int, field: String)

  def checkWinner(game: Game)(implicit s: DBSession = AutoSession): Future[Int] = ???

  def doTurn(game: Game, x: Int, y: Int)(implicit s: DBSession = AutoSession): Future[Int] = ???

}

object Helpers {

  implicit val freeParameterBinderFactory: ParameterBinderFactory[Seq[Seq[Int]]] = ParameterBinderFactory {
    value => (stmt, idx) => stmt.setString(idx, value)
  }

  implicit def packFields(fieldList: Seq[Seq[Int]]): String = {
    fieldList.map(_.mkString(",")).mkString(";")
  }

  implicit def unPackField(str: String): Seq[Seq[Int]] = {
    str.split(";").map(_.split(",").map(_.toInt).toSeq).toSeq
  }

}


object GameInfo extends SQLSyntaxSupport[GameInfo] {

  import Helpers._

  def apply(r: ResultName[GameInfo])(rs: WrappedResultSet) =
    new GameInfo(
      rs.int(r.id),
      size = List(rs.int("sizex"), rs.int("sizey")),
      finished = rs.boolean("is_finished"),
      rs.string(r.won),
      players = List(rs.string("player_one"),rs.string("player_two")),
      next_step = r.next_step.toString(),
      rs.int(r.crosses_length_to_win),
      steps = rs.int("step_counter"),
      field = rs.string("gamefield")
    )
  override val tableName = "t_game"

  private val m = syntax

  def findById(id : Int)(implicit s: DBSession = AutoSession): Future[Option[GameInfo]] = Future {
    val game: Option[GameInfo] = sql"""
        select g.id, g.sizex, g.sizey, g.crosses_length_to_win, IFNULL(u1.username,'') as next_step,
         u2.username as player_one, u3.username as player_two, g.first_step, IFNULL(u4.username,'') as won, g.is_finished,
         g.step_counter, gamefield
        from t_game g
        left join t_user u1 on g.next_step = u1.id
        left join t_user u2 on g.player_one = u2.id
        left join t_user u3 on g.player_two = u3.id
        left join t_user u4 on g.won = u4.id
        where g.id = ${id}
        """
      .stripMargin
      .map(rs =>
        new GameInfo(
          rs.int("id"),
          size = List(rs.int("sizex"), rs.int("sizey")),
          finished = rs.boolean("is_finished"),
          won =  rs.string("won"),
          players = List(rs.string("player_one"),rs.string("player_two")),
          next_step = rs.string("next_step"),
          rs.int("crosses_length_to_win"),
          steps = rs.int("step_counter"),
          field = rs.string("gamefield")
        )
      ).headOption().apply()

    game

  }


  def list(limit: Int, offset : Int)(implicit s: DBSession = AutoSession): Future[List[GameInfo]] = Future {
    val gameList: List[GameInfo] = sql"""
        select g.id, g.sizex, g.sizey, g.crosses_length_to_win, u1.username as next_step,
         u2.username as player_one, u3.username as player_two, g.first_step, u4.username as won, g.is_finished,
         g.step_counter, gamefield
        from t_game g
        left join t_user u1 on g.next_step = u1.id
        left join t_user u2 on g.player_one = u2.id
        left join t_user u3 on g.player_two = u3.id
        left join t_user u4 on g.won = u4.id
        order by g.id
        limit ${limit} offset ${offset}
        """
      .stripMargin
      .map(rs =>
          new GameInfo(
            rs.int("id"),
            size = List(rs.int("sizex"), rs.int("sizey")),
            finished = rs.boolean("is_finished"),
            rs.string("won"),
            players = List(rs.string("player_one"),rs.string("player_two")),
            next_step = rs.string("next_step"),
            rs.int("crosses_length_to_win"),
            steps = rs.int("step_counter"),
            field = rs.string("gamefield")
          )
      )
      .list
      .apply()
    gameList

  }

  def validateGameTurnRequest(sess : Option[String], gameTurnRequest: GameTurnRequest, gameInfo: GameInfo): Boolean = true
  /*
  {

    if (gameInfo.field(gameTurnRequest.step.head)(gameTurnRequest.step(1)) == 0) true
    else throw  CellNonEmptyException("Cell is already occupied")

  }
  */

  def changeTurn(gameId : Int, id : Int, endFlag: Int)(implicit s: DBSession = AutoSession): Future[Boolean] = Future {

  if (endFlag == 0) {
    id match{
      case 1 => sql"update t_game set next_step = player_two where id = ${gameId}".update.apply() == 1
      case 2 => sql"update t_game set next_step = player_one where id = ${gameId}".update.apply() == 1
      case _ => false
    }
  }
    sql"update t_game set step_counter = step_counter+1 where id = ${gameId}".update.apply() == 1
  }

  def doTurn (sess: Option[String], id :Int, gameTurnRequest: GameTurnRequest, player : Int)
             (implicit s: DBSession = AutoSession): Future[Option[GameInfo]] = {

    findById(id).flatMap{ opG =>
      val x = opG.filter(q => validateGameTurnRequest(sess, gameTurnRequest,q))
     //   .filter(gameInfo => gameInfo.field(gameTurnRequest.step.head)(gameTurnRequest.step(1)) == 0)
        .map{ gameInfo =>
          Future.sequence(gameInfo.players.map(User.findById(_).map(_.map(_.id))))
            .map(_.filter(_.isDefined).map(_.get))
            .map(_.indexOf(player) + 1)
            .flatMap{ playerChoice =>
              if (playerChoice < 1) throw  InvalidPlayerException("invalid player for game")

              User.findById(gameInfo.next_step).map(_.filter(_.id != player)).map{
                case Some(user) => throw WrongTurnException("It is not current player turn")
                case None => None
              }.flatMap { _ =>

                if (gameTurnRequest.step.head > (gameInfo.size.head-1) || gameTurnRequest.step(1) > (gameInfo.size(1)-1)
                   || gameTurnRequest.step.head < 0 || gameTurnRequest.step(1) < 0)
                  throw  StepOutOfDeckException("Step parameters are out of field bounds")

                if (gameInfo.field(gameTurnRequest.step(1))(gameTurnRequest.step.head) != 0)
                  throw  CellNonEmptyException("Cell is already occupied")

                val fromSeq = packFields(gameInfo.field)
                val checkPos = gameTurnRequest.step(1) * gameInfo.size.head * 2 + gameTurnRequest.step.head * 2
                val res = unPackField(fromSeq.substring(0, checkPos) + playerChoice.toString + fromSeq.substring(checkPos + 1))
                val endFlag = checkEnd(gameInfo, res, player, gameTurnRequest, playerChoice)
                updateField(gameInfo.id, res, player, endFlag)
                  .flatMap { updateStatus =>
                    if (updateStatus) {
                      Future.successful(changeTurn(gameInfo.id, playerChoice: Int, endFlag)) // returns Future[Boolean]
                      Future.successful(true)
                    }
                    else Future.failed(throw new Exception("Updating turn failed"))
                  }
              }
            }
        }
      Future.sequence(Option.option2Iterable(x)).map(_.headOption)
    }.map(_.filter(p => p))
      .flatMap( _ => findById(id))

/*
    findById(id).map{
        case Some(gameInfo) if validateGameTurnRequest(sess, gameTurnRequest) =>

          val turnOn = gameInfo.field(gameTurnRequest.step.head)(gameTurnRequest.step(1))

          if (turnOn == 0) {
            val fromSeq = packFields(gameInfo.field)

            val checkPos = gameTurnRequest.step(1)*(gameInfo.size.head+1)*2 + gameTurnRequest.step.head*2
            val res = unPackField(fromSeq.substring(0,checkPos) + "1" + fromSeq.substring(checkPos +1))

            updateField(gameInfo.id, res)

          } else {
            val res = "-1"
          }

       // Some(gameInfo)
        case None => "-1"

    }.flatMap(p => findById(id).map {
      case Some(newgame) => Some(newgame)
      case None => None
    })
    */

  }

  def updateField(id : Int, newField : String, player: Int, endFlag: Int)(implicit s: DBSession = AutoSession): Future[Boolean] = Future {

    if (endFlag == -1) { // It's a draw
      sql"update t_game set gamefield = ${newField}, is_finished = 1, next_step = NULL where id = ${id}".update.apply() == 1

    } else if (endFlag == player) { // Has winner
      sql"update t_game set gamefield = ${newField}, is_finished = 1, next_step = NULL, won = ${player} where id = ${id}".update.apply() == 1

    } else { // Normal turn
      sql"update t_game set gamefield = ${newField} where id = ${id}".update.apply() == 1
    }

  }

  def checkEnd(gameInfo: GameInfo, res: Seq[Seq[Int]], player: Int, gameTurnRequest: GameTurnRequest, playerChoice: Int) : Int = {

    var horleft : Int = 0
    var horright: Int = 0
    val x = gameTurnRequest.step(1)
    val y = gameTurnRequest.step.head
    val hor = gameInfo.field(y).toIndexedSeq

    println(x)
    println(y)
    println(hor)

    for (i <- hor.length to 0) {
      println("*")
      println(hor(i))
    }

  //  0 until res(x)(y)
    0

  }

}


package course.scala

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.{Directive0, Route}
import akka.http.scaladsl.server.Directives._
import course.scala.models.UserSession.UserNotFoundException
import course.scala.models._
import course.scala.services._
import scalikejdbc.DB
import spray.json._
import scala.util.control.Exception

import course.scala.models.{ InvalidPlayerException, WrongTurnException, CellNonEmptyException,StepOutOfDeckException}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

trait GeneralJsonSupport extends DefaultJsonProtocol with SprayJsonSupport {

  implicit val userFormat = jsonFormat6(User.apply)
  implicit val userregFormat = jsonFormat2(UserReg.apply)
  implicit val userinfoFormat = jsonFormat4(UserInfo.apply)
  // implicit val usersessionFormat = jsonFormat6(UserSession.apply)
  // implicit val gameFormat = jsonFormat14(Game.apply)
  implicit val gameinfoFormat = jsonFormat9(GameInfo.apply)
  implicit val gameregFormat = jsonFormat4(GameReg.apply)
  implicit val gameTurnRequestFormat = jsonFormat1(GameTurnRequest.apply)

}

trait WithAdmin {
  def withAdmin: Directive0 = optionalHeaderValueByName("admin").flatMap {
    case Some(k)  => pass
    case _ => complete(StatusCodes.Unauthorized)
  }
}


trait Api extends GeneralJsonSupport with WithAdmin {

  val userService: UserService
  val gameService: GameService
  val debugService: DebugService

  val route =
    pathPrefix("user") {
       get {
            pathEnd {
              withActiveSession {
                parameterMap { paramsMap => {
                  val offset = paramsMap.getOrElse("offset", "-1").toInt
                  val limit = paramsMap.getOrElse("limit", "-1").toInt

                  if (offset < 0 || limit <= 0) {
                    complete(StatusCodes.BadRequest)
                  } else {
                    onSuccess(userService.listUser(limit, offset - 1)) {
                      case Some(userList) =>
                        complete(StatusCodes.OK -> userList.toJson)
                      case None =>
                        complete(StatusCodes.NotFound)
                    }
                  }
                }
                }
              }

            } ~ // pathPrefix("user") {
                path(Segment) { username =>
                  withActiveSession {
                      //    paramsMap("offset").toInt
                      onSuccess(userService.getUserInfo(username)) {
                        case Some(userInfo) =>
                          complete(StatusCodes.OK -> JsObject(userInfo.toJson.asJsObject.fields))
                        case None =>
                          complete(StatusCodes.NotFound)
                      }
                  }
                }
      // }
    } ~  post {
            pathPrefix("login") {
               pathEnd {
                  entity(as[UserReg]) { userReg =>
                    onComplete(userService.login(userReg)) {
                      case Success(uid) => complete(StatusCodes.OK -> JsObject(Map("session" -> uid.toJson)))
                      case Failure(u: UserNotFoundException) => complete(StatusCodes.Forbidden)
                      case _ => complete(StatusCodes.InternalServerError)
                    }
                  }

                }

          } } ~ post {

          pathPrefix("logout") {
                withActiveSession {
                  optionalHeaderValueByName("session") { session =>
                    onSuccess(userService.logout(session.getOrElse("false"))) {
                      case true => complete(StatusCodes.OK)
                      case _ =>  complete(StatusCodes.BadRequest)
                    }
                  }
                }
            } ~ entity(as[UserReg]) { userReg =>
              if (userReg.username.length < 3 || userReg.username.length > 20
                || userReg.password.length < 8 || userReg.password.length > 100)
                complete(StatusCodes.BadRequest)
              else {
                onSuccess(userService.registerUser(userReg)) {
                  case 1 => complete(StatusCodes.OK)
                  case -1 => complete(StatusCodes.Conflict)
                  case _ => complete(StatusCodes.BadRequest)
                }
              }
        }

        }

      } ~ pathPrefix("debug") {
              pathPrefix("reset") {
                withAdmin {
                  post {
                  // pathEnd {
                    onSuccess(debugService.clearAll()) {
                      case true => complete(StatusCodes.OK)
                      case _ => complete(StatusCodes.NotFound)
                   }
                  // }
                  }
              }

              }
          }  ~   pathPrefix("game") {
      get {
        pathEnd {
          withActiveSession {
            parameterMap { paramsMap => {
              val offset = paramsMap.getOrElse("offset", "-1").toInt
              val limit = paramsMap.getOrElse("limit", "-1").toInt

              if (offset < 0 || limit <= 0) {
                complete(StatusCodes.BadRequest)
              } else {
                onSuccess(gameService.listGame(limit, offset - 1)) {
                  case gameList if gameList.nonEmpty =>
                    complete(StatusCodes.OK -> gameList.toJson)
                  case _ =>
                    complete(StatusCodes.NotFound)
                }
              }
            }
            }
          }

        } ~ path(Segment) { game =>
          withActiveSession {
            onSuccess(gameService.getGameInfo(game.toInt)) {
              case Some(gameInfo) =>
                complete(StatusCodes.OK -> JsObject(gameInfo.toJson.asJsObject.fields))
              case None =>
                complete(StatusCodes.NotFound)
            }
          }
        }
      } ~ post {
        path(Segment) { gameId =>
          entity(as[GameTurnRequest]) { gameTurnRequest: GameTurnRequest =>
            withActiveSession {
              optionalHeaderValueByName("session") { sess =>

                onComplete(gameService.doTurn(sess, gameId.toInt, gameTurnRequest)) {
                  case Success(Some(gameInfo)) =>
                    complete(StatusCodes.OK -> JsObject(gameInfo.toJson.asJsObject.fields))
                  case Success(None) => complete(StatusCodes.NotFound)
                  case Failure(ex: WrongTurnException) => complete(StatusCodes.BadRequest)
                  case Failure(ex: StepOutOfDeckException) => complete(StatusCodes.BadRequest)
                  case Failure(ex: InvalidPlayerException) => complete(StatusCodes.Forbidden)
                  case Failure(ex: CellNonEmptyException) => complete(StatusCodes.Conflict)
                  case Failure(_) => complete(StatusCodes.InternalServerError)
                }

              }
            }
          }

        }
      } ~ post {
        entity(as[GameReg]) { gameReg =>
          withActiveSession {
            optionalHeaderValueByName("session") { sess =>


            if (gameReg.size.head < 3 || gameReg.size.head > 9
              || gameReg.size(1) < 3 || gameReg.size(1) > 9
              || gameReg.crosses_length_to_win < 3
              || gameReg.crosses_length_to_win > gameReg.size.min
            )
              complete(StatusCodes.BadRequest)
            else {
              onComplete(gameService.registerGame(sess, gameReg)) {
                case Success(v) => complete(StatusCodes.OK)
                case Failure(ex: OpponentNotFoundException) => complete(StatusCodes.BadRequest)
                case Failure(_) => complete(StatusCodes.BadRequest)
              }

            }
          }
        }
      }
    }
       // ~ post {
/*
          entity(as[GameReg]) { gameReg =>
          if (userReg.username.length < 3 || userReg.username.length > 20
            || userReg.password.length < 8 || userReg.password.length > 100)
            complete(StatusCodes.BadRequest)
          else {
            onSuccess(userService.registerUser(userReg)) {
              case 1 => complete(StatusCodes.OK)
              case -1 => complete(StatusCodes.Conflict)
              case _ => complete(StatusCodes.BadRequest)
            }
          }
        }

      } */

    }

  private def withActiveSession(route: Route) = {
    optionalHeaderValueByName("session") { session =>
      val isValidFuture = userService.checkSession(session)
      onSuccess(isValidFuture) { isValidSession =>
        if (isValidSession) {
            route
        }
        else {
          complete(StatusCodes.Unauthorized)
        }
      }
    }
  }

}

package course.scala.services

import course.scala.models._
import course.scala.{TictacDb, models}
import scalikejdbc.DB

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

trait UserService {
  // def createUser(user: User): Future[Unit]
  def registerUser(userreg: UserReg): Future[Int]
  def getUser(username: String): Future[Option[User]]
  def getUserInfo(username: String) :Future[Option[UserInfo]]
  def listUser (limit: Int, offset: Int) : Future[Option[List[UserInfo]]]
  def checkSession (uid: Option[String]) : Future[Boolean]
  def login(userreg: UserReg): Future[String]
  def logout(uid: String): Future[Boolean]
}

class UserServiceImpl extends UserService {
  TictacDb.init()
  Await.result(TictacDb.createTicTacDB(), Duration.Inf)
  Await.result(TictacDb.addTicTacDB(), Duration.Inf)
   // override def createUser(user: User): Future[Unit] =  DB.futureLocalTx(implicit session => User.register(user).map(_ => ()))

  override def registerUser(userreg: UserReg): Future[Int] =  DB.futureLocalTx(implicit session => User.register(userreg))

  override def getUser(username: String): Future[Option[User]] = ??? // DB.futureLocalTx(implicit session => User.findById(username))

  override def getUserInfo(username: String): Future[Option[UserInfo]] = DB.futureLocalTx(implicit session => UserInfo.findById(username))

  override def listUser (limit: Int, offset: Int) : Future[Option[List[UserInfo]]] = DB.futureLocalTx(implicit session => UserInfo.list(limit,offset))

  override def checkSession(uid: Option[String]): Future[Boolean] = DB.futureLocalTx(implicit session =>
    UserSession.checkSession(uid).map(n => n.isDefined))


  def login(userreg: UserReg): Future[String] = DB.futureLocalTx(implicit session => UserSession.logIn(userreg))

  def logout(uid: String): Future[Boolean] = DB.futureLocalTx(implicit session =>
    UserSession.closeSession(uid))
}

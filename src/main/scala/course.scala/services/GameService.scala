package course.scala.services

import akka.actor.FSM.Failure
import course.scala.models.{Game, _}
import course.scala.{TictacDb, models}
import scalikejdbc.DB

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.Success

trait GameService {
/*
  def create (game :Game) : Future[Unit]
  def packField (seqField: Seq[Int] ): String
  def unpackField (strField: String) : Seq[Int]
  def checkWon (gameId : Int) : Boolean
  def getGame(id: Int): Future[Option[Game]]
  def getGameInfo(id: Int): Future[Option[GameInfo]]
  */

  def doTurn(sess : Option[String], id: Int, gameTurnRequest: GameTurnRequest) : Future[Option[GameInfo]]

  def listGame (limit: Int, offset: Int) : Future[List[GameInfo]]

  def getGameInfo(id: Int): Future[Option[GameInfo]]

  def registerGame(sess: Option[String], gamereg: GameReg): Future[Int]

}

class GameServiceImpl extends GameService {

  override def listGame (limit: Int, offset: Int) : Future[List[GameInfo]] =
    DB.futureLocalTx(implicit session => GameInfo.list(limit,offset))

  override def getGameInfo(id: Int): Future[Option[GameInfo]] =
    DB.futureLocalTx(implicit session => GameInfo.findById(id))

  override def doTurn(sess: Option[String], id: Int, gameTurnRequest: GameTurnRequest) : Future[Option[GameInfo]] =
    DB.futureLocalTx(implicit session => {

      def player: Future[Int] = UserSession.findByUid(sess.getOrElse("")).map(ns => ns.get.user_id)

      for (n1 <- player;
           n2 <- GameInfo.doTurn(sess, id, gameTurnRequest, n1))
        yield n2
    }

    //  val player = UserSession.findByUid(sess).flatMap( mySess => UserInfo.findById(mySess.get.user_id)).flatMap(_).



    //  		  GameInfo.doTurn(sess, id, gameTurnRequest).flatMap( p =>
    //  		    GameInfo.findById(id))

    )

    def registerGame(sess: Option[String], gamereg: GameReg): Future[Int] = DB.futureLocalTx(implicit session => Game.register(sess, gamereg))

}

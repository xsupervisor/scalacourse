package course.scala.services

import course.scala.models.Debug
import scalikejdbc.DB
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future}

trait DebugService {

  def clearAll(): Future[Boolean]

}

class DebugServiceImpl extends DebugService {

   override def clearAll() : Future[Boolean] = DB.futureLocalTx(implicit session => Debug.clearDB())

}

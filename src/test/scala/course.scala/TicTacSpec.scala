package course.scala

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.{ModeledCustomHeader, ModeledCustomHeaderCompanion}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{Matchers, WordSpec}
import akka.http.scaladsl.server._
import Directives._
import course.scala.models.{User, UserInfo, UserReg}

import scala.collection.mutable
import scala.concurrent.Future
import scala.util.Try
import spray.json._

class TicTacSpec extends WordSpec with Matchers with ScalatestRouteTest with GeneralJsonSupport {

  private val testUser = User(1, "vasya", "test",1,3,1)
  private val testUserInfo = UserInfo("vasya", true, 3,1)
  private val testUserForInsert = UserReg("terminator", "turboflight")

  object session extends ModeledCustomHeaderCompanion[session] {
    override val name                 = "session"
    override def parse(value: String) = Try(new session(value))
  }

  final class session(override val value: String) extends ModeledCustomHeader[session] {
    override def companion         = session
    override def renderInRequests  = true
    override def renderInResponses = false
  }

  object admin extends ModeledCustomHeaderCompanion[admin] {
    override val name                 = "admin"
    override def parse(value: String) = Try(new admin(value))
  }

  final class admin(override val value: String) extends ModeledCustomHeader[admin] {
    override def companion         = admin
    override def renderInRequests  = true
    override def renderInResponses = false
  }

  "TicTac service" should {

    "[USER INFO] return user for GET requests to the /user/:username path" in {
      Get("/user/vasya") ~> session("34323432") ~> WebServer.route ~> check {
        status shouldEqual StatusCodes.OK
        responseAs[String].parseJson.convertTo[UserInfo] shouldEqual testUserInfo
      }
    }

    "[USER INFO] return an error for GET requests without 'auth' header to the /user/:username path" in {
      Get("/user/vasya") ~> WebServer.route ~> check {
        status shouldEqual StatusCodes.Unauthorized
        responseAs[String] shouldEqual "Authentication is possible but has failed or not yet been provided."
      }
    }

    "[USER INFO] return an error for GET requests with invalid 'auth' header to the /user/:username path" in {
      Get("/user/vasya") ~> session("321") ~> WebServer.route ~> check {
        status shouldEqual StatusCodes.Unauthorized
        responseAs[String] shouldEqual "Authentication is possible but has failed or not yet been provided."
      }
    }

    "[USER INFO] return an error for GET requests to the /user/:username path for not existed id" in {
      Get("/user/vasyator") ~> session("34323432") ~> WebServer.route ~> check {
        status shouldEqual StatusCodes.NotFound

      }
      Post("/user", testUserForInsert) ~> WebServer.route ~> check {
        status shouldEqual StatusCodes.OK
      }
    }

    "[USER INSERT] return OK for POST requests to the /user/ path" in {
      Post("/user", testUserForInsert) ~> WebServer.route ~> check {
        status shouldEqual StatusCodes.OK
      }
    }

    "[DEBUG RESET] return auth error for POST requests to the /debug/reset without ADMIN header " in {
      Post("/debug/reset") ~> WebServer.route ~> check {
        status shouldEqual StatusCodes.Unauthorized
      }
    }

    "[DEBUG RESET] return OK for POST requests to the /debug/reset with empty ADMIN header " in {
      Post("/debug/reset") ~> admin("") ~> WebServer.route ~> check {
        status shouldEqual StatusCodes.OK
      }
    }

    "[USER INFO] return user for GET requests to the /user/:username path second try" in {
      Get("/user/vasya") ~> session("34323432") ~> WebServer.route ~> check {
        status shouldEqual StatusCodes.Unauthorized
      }
    }

    "[USER INSERT] return OK for POST requests to the /user/ path second try" in {
      Post("/user", testUserForInsert) ~> session("34323432") ~> WebServer.route ~> check {
        status shouldEqual StatusCodes.OK
      }
    }

    "[DEBUG RESET] return OK for POST requests to the /debug/reset with any ADMIN header " in {
      Post("/debug/reset") ~> admin("admin") ~> WebServer.route ~> check {
        status shouldEqual StatusCodes.OK
      }
    }

    "[USER INFO] return user for GET requests to the /user/:username path 3rd try" in {
      Get("/user/vasya") ~> session("34323432") ~> WebServer.route ~> check {
        status shouldEqual StatusCodes.Unauthorized
      }
    }

    "[USER INSERT] return OK for POST requests to the /user/ path once again" in {
      Post("/user", testUserForInsert)  ~> WebServer.route ~> check {
        status shouldEqual StatusCodes.OK
      }
    }

  }

  "Game service" should {


  }
}